from django.db import models
from django.utils import timezone

STATUSES = (
    (1, 'Activo'),
    (2, 'Inactivo'),
)

class Usuario(models.Model):
	nombre = models.CharField(max_length = 200)
	apellido = models.CharField(max_length = 200)
	username = models.CharField(max_length = 100)
	estado = models.PositiveSmallIntegerField(choices = STATUSES, default = 1)
	fecha_creacion = models.DateTimeField(default = timezone.now)

	def nuevo_usuario(self):
		self.estado = 1
		self.fecha_creacion = timezone.now()
		self.save()
	
	def __str__(self):
		return self.username
