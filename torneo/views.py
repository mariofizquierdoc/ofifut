from django.shortcuts import render
from django.utils import timezone
from .models import Usuario

def usuario_list(request):
	usuarios = Usuario.objects.filter(fecha_creacion__lte = timezone.now()).order_by('fecha_creacion')
	return render(request, 'torneo/usuario_list.html', {'usuarios': usuarios})